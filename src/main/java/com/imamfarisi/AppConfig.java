package com.imamfarisi;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.imamfarisi.model.Universitas;

@Configuration
@PropertySources({ 
	@PropertySource("classpath:hibernate.properties"),
	@PropertySource("classpath:datasource.properties") 
				})
@EnableTransactionManagement
@ComponentScan(basePackages = {"com.imamfarisi"})
public class AppConfig {

	@Autowired
	private Environment env;

	@Autowired
	private SessionFactory sessionFactory;

	@Bean
	public InternalResourceViewResolver getIRV() {
		InternalResourceViewResolver irv = new InternalResourceViewResolver();
		irv.setPrefix("/WEB-INF/views/");
		irv.setSuffix(".jsp");
		return irv;
	}

	@Bean
	public Universitas getUniv() {
		Universitas univ = new Universitas();
		univ.setId(1L);
		univ.setNama("Telkom University");
		return univ;
	}

	@Bean
	public DataSource getDataSource() {
		DriverManagerDataSource dm = new DriverManagerDataSource();
		dm.setDriverClassName(env.getProperty("db.driverClassName"));
		dm.setUrl(env.getProperty("db.url"));
		dm.setUsername(env.getProperty("db.username"));
		dm.setPassword(env.getProperty("db.password"));

		return dm;
	}

	@Bean
	public LocalSessionFactoryBean getSessionFactory() {
		LocalSessionFactoryBean ls = new LocalSessionFactoryBean();
		ls.setDataSource(getDataSource());

		Properties p = new Properties();
		p.setProperty("hibernate.dialect", env.getProperty("hibernate.dialect"));
		p.setProperty("hibernate.show_sql", env.getProperty("hibernate.show_sql"));
		ls.setHibernateProperties(p);

		ls.setPackagesToScan("com.imamfarisi.model");

		return (LocalSessionFactoryBean) ls;
	}

	@Bean
	public HibernateTransactionManager getTransManager() {
		return new HibernateTransactionManager(sessionFactory);
	}

}
