package com.imamfarisi.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Universitas {

	@Id
	private Long id;

	private String nama;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	@Override
	public String toString() {
		return "ID : " + id + ", NAMA : " + nama;
	}

}
