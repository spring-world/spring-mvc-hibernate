package com.imamfarisi.dao;

import java.util.List;

import com.imamfarisi.model.Universitas;

public interface UniversitasDAO {

    void insertData(Universitas data);

    void updateData(Universitas data);

    void deleteData(Universitas data);

    void deleteAllData();

    List<Universitas> getAllData();
}
