package com.imamfarisi.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.imamfarisi.model.Universitas;

@Repository
public class UniversitasDAOImpl implements UniversitasDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public void insertData(Universitas data) {
		sessionFactory.getCurrentSession().save(data);
	}

	public void updateData(Universitas data) {
		sessionFactory.getCurrentSession().merge(data);
	}

	public void deleteData(Universitas data) {
		sessionFactory.getCurrentSession().remove(data);
	}

	public void deleteAllData() {
		String sql = "DELETE FROM Universitas";
		sessionFactory.getCurrentSession().createQuery(sql).executeUpdate();
	}

	public List<Universitas> getAllData() {
		List<Universitas> listUniv = new ArrayList<>();
		listUniv = sessionFactory.getCurrentSession().createQuery("from Universitas", Universitas.class).list();
		return listUniv;
	}

}
