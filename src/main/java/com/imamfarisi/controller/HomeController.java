package com.imamfarisi.controller;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.imamfarisi.model.Universitas;
import com.imamfarisi.service.UniversitasService;

@Controller
public class HomeController {

	@Autowired
	private UniversitasService univService;

	@Autowired
	private Universitas univ;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		univService.deleteAllData();
		univService.insertData(univ);

		model.addAttribute("data_before", univ.toString());

		univ.setNama("ITB");
		univService.updateData(univ);

		model.addAttribute("data_after", univ.toString());

		List<Universitas> listUniv = univService.getAllData();
		listUniv.forEach(System.out::println);

		return "home";
	}

}
