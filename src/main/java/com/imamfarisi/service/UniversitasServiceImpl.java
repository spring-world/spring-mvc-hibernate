package com.imamfarisi.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.imamfarisi.dao.UniversitasDAO;
import com.imamfarisi.model.Universitas;

@Transactional
@Service
public class UniversitasServiceImpl implements UniversitasService {

	@Autowired
	private UniversitasDAO universitasDAO;

	public void insertData(Universitas data) {
		universitasDAO.insertData(data);
	}

	public void updateData(Universitas data) {
		universitasDAO.updateData(data);
	}

	public void deleteData(Universitas data) {
		universitasDAO.deleteData(data);
	}

	public void deleteAllData() {
		universitasDAO.deleteAllData();
	}

	public List<Universitas> getAllData() {
		return universitasDAO.getAllData();
	}

}
